USE [master]
GO

DECLARE @killConn varchar(8000) = '';  
SELECT @killConn = @killConn + 'kill ' + CONVERT(varchar(5), session_id) + ';'  
FROM sys.dm_exec_sessions
WHERE database_id  = db_id('LogDB')

EXEC(@killConn);


/****** Object:  Database [LogDB]    Script Date: 24/10/2019 13:38:12 ******/
IF EXISTS (SELECT 1 FROM master.dbo.sysdatabases WHERE ('[' + name + ']' = '[LogDB]' OR name = '[LogDB]'))
BEGIN
	DROP DATABASE [LogDB]
END
GO

/****** Object:  Database [LogDB]    Script Date: 24/10/2019 13:38:12 ******/
CREATE DATABASE [LogDB]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'LogDB', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.MSSQLSERVER\MSSQL\DATA\LogDB.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'LogDB_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.MSSQLSERVER\MSSQL\DATA\LogDB_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
GO

ALTER DATABASE [LogDB] SET COMPATIBILITY_LEVEL = 140
GO

IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [LogDB].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO

ALTER DATABASE [LogDB] SET ANSI_NULL_DEFAULT OFF 
GO

ALTER DATABASE [LogDB] SET ANSI_NULLS OFF 
GO

ALTER DATABASE [LogDB] SET ANSI_PADDING OFF 
GO

ALTER DATABASE [LogDB] SET ANSI_WARNINGS OFF 
GO

ALTER DATABASE [LogDB] SET ARITHABORT OFF 
GO

ALTER DATABASE [LogDB] SET AUTO_CLOSE OFF 
GO

ALTER DATABASE [LogDB] SET AUTO_SHRINK OFF 
GO

ALTER DATABASE [LogDB] SET AUTO_UPDATE_STATISTICS ON 
GO

ALTER DATABASE [LogDB] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO

ALTER DATABASE [LogDB] SET CURSOR_DEFAULT  GLOBAL 
GO

ALTER DATABASE [LogDB] SET CONCAT_NULL_YIELDS_NULL OFF 
GO

ALTER DATABASE [LogDB] SET NUMERIC_ROUNDABORT OFF 
GO

ALTER DATABASE [LogDB] SET QUOTED_IDENTIFIER OFF 
GO

ALTER DATABASE [LogDB] SET RECURSIVE_TRIGGERS OFF 
GO

ALTER DATABASE [LogDB] SET  DISABLE_BROKER 
GO

ALTER DATABASE [LogDB] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO

ALTER DATABASE [LogDB] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO

ALTER DATABASE [LogDB] SET TRUSTWORTHY OFF 
GO

ALTER DATABASE [LogDB] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO

ALTER DATABASE [LogDB] SET PARAMETERIZATION SIMPLE 
GO

ALTER DATABASE [LogDB] SET READ_COMMITTED_SNAPSHOT OFF 
GO

ALTER DATABASE [LogDB] SET HONOR_BROKER_PRIORITY OFF 
GO

ALTER DATABASE [LogDB] SET RECOVERY FULL 
GO

ALTER DATABASE [LogDB] SET  MULTI_USER 
GO

ALTER DATABASE [LogDB] SET PAGE_VERIFY CHECKSUM  
GO

ALTER DATABASE [LogDB] SET DB_CHAINING OFF 
GO

ALTER DATABASE [LogDB] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO

ALTER DATABASE [LogDB] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO

ALTER DATABASE [LogDB] SET DELAYED_DURABILITY = DISABLED 
GO

ALTER DATABASE [LogDB] SET QUERY_STORE = OFF
GO

USE [LogDB]
GO

ALTER DATABASE SCOPED CONFIGURATION SET IDENTITY_CACHE = ON;
GO

ALTER DATABASE SCOPED CONFIGURATION SET LEGACY_CARDINALITY_ESTIMATION = OFF;
GO

ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET LEGACY_CARDINALITY_ESTIMATION = PRIMARY;
GO

ALTER DATABASE SCOPED CONFIGURATION SET MAXDOP = 0;
GO

ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET MAXDOP = PRIMARY;
GO

ALTER DATABASE SCOPED CONFIGURATION SET PARAMETER_SNIFFING = ON;
GO

ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET PARAMETER_SNIFFING = PRIMARY;
GO

ALTER DATABASE SCOPED CONFIGURATION SET QUERY_OPTIMIZER_HOTFIXES = OFF;
GO

ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET QUERY_OPTIMIZER_HOTFIXES = PRIMARY;
GO

ALTER DATABASE [LogDB] SET  READ_WRITE 
GO


