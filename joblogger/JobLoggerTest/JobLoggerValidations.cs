﻿using System;
using System.Collections.Generic;
using JobLogger.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace JobLoggerTest
{
    [TestClass]
    public class JobLoggerValidations
    {
        #region DatabaseTesting
        [TestMethod]
        public void CheckDataBaseConnection()
        {
            List<int> logTypesAllowed = new List<int>();
            logTypesAllowed.Add(1);
            logTypesAllowed.Add(2);
            logTypesAllowed.Add(3);
            JobLoggerLogic jobLoggerLogic = new JobLoggerLogic(false, false, true, logTypesAllowed);
            jobLoggerLogic.LogMessage("This is an log message", new LogType { Id = 1, Name = "Message", Description = "Log Message: " });
        }

        [TestMethod]
        public void CheckDataBaseWarningMessage()
        {
            List<int> logTypesAllowed = new List<int>();
            logTypesAllowed.Add(1);
            logTypesAllowed.Add(2);
            logTypesAllowed.Add(3);
            JobLoggerLogic jobLoggerLogic = new JobLoggerLogic(false, false, true, logTypesAllowed);
            jobLoggerLogic.LogMessage("This is a warning message", new LogType { Id = 2, Name = "Warning", Description = "Warning Message: " });
        }

        [TestMethod]
        public void CheckDataBaseWarningAndErrorMessage()
        {
            List<int> logTypesAllowed = new List<int>();
            logTypesAllowed.Add(1);
            logTypesAllowed.Add(2);
            logTypesAllowed.Add(3);
            JobLoggerLogic jobLoggerLogic = new JobLoggerLogic(false, false, true, logTypesAllowed);
            jobLoggerLogic.LogMessage("This is a warning message", new LogType { Id = 2, Name = "Warning", Description = "Warning Message: " });
            jobLoggerLogic.LogMessage("This is a error message", new LogType { Id = 3, Name = "Error", Description = "Error Message: " });
        }

        [TestMethod]
        public void CheckDataBaseWarningAndErrorMessageWhithoutWarningAllowed()
        {
            List<int> logTypesAllowed = new List<int>();
            logTypesAllowed.Add(3);
            JobLoggerLogic jobLoggerLogic = new JobLoggerLogic(false, false, true, logTypesAllowed);
            jobLoggerLogic.LogMessage("This is a warning message", new LogType { Id = 2, Name = "Warning", Description = "Warning Message: " });
            jobLoggerLogic.LogMessage("This is a error message", new LogType { Id = 3, Name = "Error", Description = "Error Message: " });
        }
        #endregion

        #region ConsoleTesting
        [TestMethod]
        public void CheckConsoleLoggin()
        {
            List<int> logTypesAllowed = new List<int>();
            logTypesAllowed.Add(1);
            logTypesAllowed.Add(2);
            logTypesAllowed.Add(3);
            JobLoggerLogic jobLoggerLogic = new JobLoggerLogic(false, true, false, logTypesAllowed);
            jobLoggerLogic.LogMessage("This is an log message", new LogType { Id = 1, Name = "Message", Description = "Log Message: " });
        }

        [TestMethod]
        public void CheckConsoleWarningMessage()
        {
            List<int> logTypesAllowed = new List<int>();
            logTypesAllowed.Add(1);
            logTypesAllowed.Add(2);
            logTypesAllowed.Add(3);
            JobLoggerLogic jobLoggerLogic = new JobLoggerLogic(false, true, false, logTypesAllowed);
            jobLoggerLogic.LogMessage("This is a warning message", new LogType { Id = 2, Name = "Warning", Description = "Warning Message: " });
        }

        [TestMethod]
        public void CheckConsoleWarningAndErrorMessage()
        {
            List<int> logTypesAllowed = new List<int>();
            logTypesAllowed.Add(1);
            logTypesAllowed.Add(2);
            logTypesAllowed.Add(3);
            JobLoggerLogic jobLoggerLogic = new JobLoggerLogic(false, true, false, logTypesAllowed);
            jobLoggerLogic.LogMessage("This is a warning message", new LogType { Id = 2, Name = "Warning", Description = "Warning Message: " });
            jobLoggerLogic.LogMessage("This is a error message", new LogType { Id = 3, Name = "Error", Description = "Error Message: " });
        }

        [TestMethod]
        public void CheckConsoleWarningAndErrorMessageWhithoutWarningAllowed()
        {
            List<int> logTypesAllowed = new List<int>();
            logTypesAllowed.Add(3);
            JobLoggerLogic jobLoggerLogic = new JobLoggerLogic(false, true, false, logTypesAllowed);
            jobLoggerLogic.LogMessage("This is a warning message", new LogType { Id = 2, Name = "Warning", Description = "Warning Message: " });
            jobLoggerLogic.LogMessage("This is a error message", new LogType { Id = 3, Name = "Error", Description = "Error Message: " });
        }

        #endregion

        #region TXTTest
        [TestMethod]
        public void CheckTXTLoggin()
        {
            List<int> logTypesAllowed = new List<int>();
            logTypesAllowed.Add(1);
            logTypesAllowed.Add(2);
            logTypesAllowed.Add(3);
            JobLoggerLogic jobLoggerLogic = new JobLoggerLogic(true, false, false, logTypesAllowed);
            jobLoggerLogic.LogMessage("This is an log message", new LogType { Id = 1, Name = "Message", Description = "Log Message: " });
        }

        [TestMethod]
        public void CheckTXTWarningMessage()
        {
            List<int> logTypesAllowed = new List<int>();
            logTypesAllowed.Add(1);
            logTypesAllowed.Add(2);
            logTypesAllowed.Add(3);
            JobLoggerLogic jobLoggerLogic = new JobLoggerLogic(true, false, false, logTypesAllowed);
            jobLoggerLogic.LogMessage("This is a warning message", new LogType { Id = 2, Name = "Warning", Description = "Warning Message: " });
        }

        [TestMethod]
        public void CheckTXTWarningAndErrorMessage()
        {
            List<int> logTypesAllowed = new List<int>();
            logTypesAllowed.Add(1);
            logTypesAllowed.Add(2);
            logTypesAllowed.Add(3);
            JobLoggerLogic jobLoggerLogic = new JobLoggerLogic(true, false, false, logTypesAllowed);
            jobLoggerLogic.LogMessage("This is a warning message", new LogType { Id = 2, Name = "Warning", Description = "Warning Message: " });
            jobLoggerLogic.LogMessage("This is a error message", new LogType { Id = 3, Name = "Error", Description = "Error Message: " });
        }

        [TestMethod]
        public void CheckTXTWarningAndErrorMessageWhithoutWarningAllowed()
        {
            List<int> logTypesAllowed = new List<int>();
            logTypesAllowed.Add(3);
            JobLoggerLogic jobLoggerLogic = new JobLoggerLogic(true, false, false, logTypesAllowed);
            jobLoggerLogic.LogMessage("This is a warning message", new LogType { Id = 2, Name = "Warning", Description = "Warning Message: " });
            jobLoggerLogic.LogMessage("This is a error message", new LogType { Id = 3, Name = "Error", Description = "Error Message: " });
        }

        #endregion
    }
}
