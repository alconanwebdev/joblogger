﻿using System;
using System.Data.SqlClient;
using System.IO;
using JobLogger.Models;
using System.Collections.Generic;

public class JobLoggerLogic
{
    private static bool _logToFile;
    private static bool _logToConsole;
    private static List<int> _logTypesAllowed;
    private static bool _logToDatabase;

    public JobLoggerLogic(bool logToFile, bool logToConsole, bool logToDatabase, List<int> logTypesAllowed)
    {
        _logToDatabase = logToDatabase;
        _logToFile = logToFile;
        _logToConsole = logToConsole;
        _logTypesAllowed = logTypesAllowed;
    }

    public void LogMessage(string logMessage, LogType logType)
    {
        logMessage.Trim();

        checkForExceptions(logMessage, logType);

        InMemoryLogTypeRepository inMemoryLogTypeRepository = new InMemoryLogTypeRepository();

        if (_logToDatabase && isLogTypeAllowed(logType))
            saveOnDatabase(logMessage, logType, inMemoryLogTypeRepository);

        if (_logToFile && isLogTypeAllowed(logType))
            saveOnFile(logMessage, logType, inMemoryLogTypeRepository);

        if (_logToConsole && isLogTypeAllowed(logType))
            showOnConsole(logMessage, logType, inMemoryLogTypeRepository);
    }

    private static bool isLogTypeAllowed(LogType logType)
    {
        return _logTypesAllowed.Contains(logType.Id);
    }

    private static void showOnConsole(string logMessage, LogType logType, InMemoryLogTypeRepository inMemoryLogTypeRepository)
    {
        if (logType.Id == inMemoryLogTypeRepository.GetErrorId())
            Console.ForegroundColor = ConsoleColor.Red;

        if (logType.Id == inMemoryLogTypeRepository.GetWarningId())
            Console.ForegroundColor = ConsoleColor.Blue;

        if (logType.Id == inMemoryLogTypeRepository.GetMessageId())
            Console.ForegroundColor = ConsoleColor.White;

        Console.WriteLine(logType.Description + logMessage + " " + DateTime.Now.ToShortDateString());
    }

    private static void saveOnFile(string logMessage, LogType logType, InMemoryLogTypeRepository inMemoryLogTypeRepository)
    {
        try
        {
            File.AppendAllText(@"C:\tmp\LogFile.txt", logMessage + " " + DateTime.Now.ToString("dd-MM-yyyy") + Environment.NewLine);
        }
        catch (Exception ex)
        {
            throw new Exception("An error has ocurred while trying to save the data, details: " + ex.Message);
        }

    }

    private static void saveOnDatabase(string logMessage, LogType logType, InMemoryLogTypeRepository inMemoryLogTypeRepository)
    {
        try
        {
            using (SqlConnection connection = new SqlConnection("Data Source=DESKTOP-LHTHMBN;Initial Catalog=LogDB;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False"))
            {
                SqlCommand command = new SqlCommand("Insert into Log Values('" + logMessage + "', " + logType.Id.ToString() + ")", connection);
                command.Connection.Open();
                command.ExecuteNonQuery();
            }
        }
        catch (Exception ex)
        {
            throw new Exception("An error has ocurred while trying to save the data, details: " + ex.Message);
        }

    }

    private static void checkForExceptions(string logMessage, LogType logType)
    {
        if (String.IsNullOrEmpty(logMessage))
            throw new Exception("Log Message must be specified");

        if (!_logToConsole && !_logToFile && !_logToDatabase)
            throw new Exception("The record target must be specified");

        if (logType == null)
            throw new Exception("The type Error, Warning or Message must be specified");

    }
}