﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JobLogger.Models
{
    interface ILogTypeRepository
    {
        LogType GetLogType(int id);
        int GetMessageId();
        int GetWarningId();
        int GetErrorId();
    }
}