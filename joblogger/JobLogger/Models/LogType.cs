﻿namespace JobLogger.Models
{
    public class LogType
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
