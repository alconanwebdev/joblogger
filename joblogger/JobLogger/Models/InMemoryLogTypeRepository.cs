﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JobLogger.Models
{
    class InMemoryLogTypeRepository : ILogTypeRepository
    {
        private List<LogType> _logTypeList;

        public InMemoryLogTypeRepository()
        {
            _logTypeList = new List<LogType>()
            {
                new LogType() { Id = 1, Name = "Message", Description = "Log Message: "},
                new LogType() { Id = 2, Name = "Warning", Description = "Warning Message: " },
                new LogType() { Id = 3, Name = "Error", Description = "Error Message: " }
            };
        }

        public int GetErrorId()
        {
            return _logTypeList.FirstOrDefault(logType => logType.Id == 3).Id;
        }


        public LogType GetLogType(int id)
        {
            return _logTypeList.FirstOrDefault(logType => logType.Id == id);
        }

        public int GetMessageId()
        {
            return _logTypeList.FirstOrDefault(logType => logType.Id == 1).Id;
        }

        public int GetWarningId()
        {
            return _logTypeList.FirstOrDefault(logType => logType.Id == 2).Id;
        }
    }
}
