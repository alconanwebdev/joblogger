﻿using JobLogger.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JobLogger
{
    class Program
    {
        static void Main(string[] args)
        {
            List<int> logTypesAllowed = new List<int>();
            logTypesAllowed.Add(3);
            JobLoggerLogic jobLoggerLogic = new JobLoggerLogic(false, true, false, logTypesAllowed);
            jobLoggerLogic.LogMessage("This is a warning message", new LogType { Id = 2, Name = "Warning", Description = "Warning Message: " });
            jobLoggerLogic.LogMessage("This is a error message", new LogType { Id = 3, Name = "Error", Description = "Error Message: " });

            Console.ReadKey();
        }
    }
}
